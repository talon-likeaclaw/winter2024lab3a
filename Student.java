public class Student {
	// Student fields
	public int age;
	public String schoolName;
	public double averageGrade;
	
	public void honourRole() {
		if (this.averageGrade > 95.00) {
			System.out.println("Honour Role");
		} else {
			System.out.println("Not Honour Role");
		}
		
	}
	
	public void onTime() {
		System.out.println("Student was on time");
	}
}
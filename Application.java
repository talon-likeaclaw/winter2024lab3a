public class Application {
	
	public static void main(String[] args) {
 		// Create Talon instance
		Student talon = new Student();
		System.out.println("Talon:");
		talon.age = 28;
		talon.schoolName = "Dawson College";
		talon.averageGrade = 96.54;
		System.out.println(talon.age);
		System.out.println(talon.schoolName);
		System.out.println(talon.averageGrade);
		talon.honourRole();
		talon.onTime();
		
		// Create Matt instance
		Student matt = new Student();
		System.out.println("Matt:");
		matt.age = 31;
		matt.schoolName = "Concordia";
		matt.averageGrade = 94.13;
		System.out.println(matt.age);
		System.out.println(matt.schoolName);
		System.out.println(matt.averageGrade);
		matt.honourRole(); 
		
		// Create student array
		Student[] section4 = new Student[3];
		section4[0] = talon;
		section4[1] = matt;
		
		// Create new student instance
		section4[2] = new Student();
		section4[2].age = 19;
		section4[2].schoolName = "Dawson College";
		section4[2].averageGrade = 84.65;
		
		System.out.println(section4[2].age);
		System.out.println(section4[2].schoolName);
		System.out.println(section4[2].averageGrade);
	}
	
}